import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Mision from '../views/Mision.vue'
import Vision from '../views/Vision.vue'
// import ProjectEmp from '../views/ProjectEmp.vue'
import ProjectInv from '../views/ProjectInv.vue'

import ProjectEnt from '../views/ProjectEnt.vue'
import Profile from '../views/Profile.vue'

import store from '../store'

// LOGIN INVESTOR
import Login from '../views/Login.vue'
//LOGIN ENTREPRENEUR
import LoginEntre from '../views/LoginEntre.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta:{
      libre:true
    }
  },
  {
    path:'/about',
    name:'About',
    component: About,
    meta:{
      libre:true
    }
  },
  {
    path:'/mision',
    name:'Mision',
    component: Mision,
    meta:{
      libre:true
    }
  },
  {
    path:'/vision',
    name:'Vision',
    component: Vision,
    meta:{
      libre:true
    }
  },
  {
    path:'/projects',
    name:'ProjectInv',
    component: ProjectInv,
    meta:{
      investor: true
    }
  },
  //ONLY PROJECT
  {
    path:'/project-ent',
    name:'ProjectEnt',
    component: ProjectEnt,
    meta:{
      entrepreneur: true 
    }
  },
  // Path login investor
  {
    path:'/login',
    name:'Login',
    component: Login,
    meta:{
      libre:true
    }
  },
  // Path login Entrepreneur
  {
    path:'/login-entrepreneur',
    name:'LoginEntre',
    component: LoginEntre,
    meta:{
      libre:true
    }
  },
  // Lazy load
  {
    path:'/profile',
    name:'Profile',
    component: ()=> import('@/views/Profile.vue'),
    meta:{
      investor:true
    }
  }
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to,from,next)=>{
  if (to.matched.some(record => record.meta.libre)){
    next();
  } else if ( store.state.userDB && store.state.userDB.role == 'INVERSIONISTA'){
    if (to.matched.some(record => record.meta.investor)){
      next();
    }
  } else if ( store.state.userDB && store.state.userDB.role == 'EMPRENDEDOR'){
    if (to.matched.some(record => record.meta.entrepreneur)){
      next();
    }/* else{
      next({name: 'Home'});
    } */
  }else{
    // next({name:'Home'})
    router.push({name:'Home'}).catch(error=>{
      if(error.name!== 'NavigationDuplicated' && !error.incluedes('Avoided redundant navigation to current location')){
        console.log(error);
      }
    })
  }
})

// router.beforeEach((to,from,next)=>{
//   console.log({to,from,next});
//   if(to.matched.some(record=> record.meta.libre)){
//     next();
//   }else if(store.state.userDB && store.state.userDB.role == 'INVERSIONISTA'){
//     if(to.matched.some(record=> record.meta.investor)){
//       next();
//     }
//   }else if(store.state.userDB && store.state.userDB.role == 'EMPRENDEDOR'){
//     if(to.matched.some(record=> record.meta.entrepreneur)){
//       next();
//     }
//   }else{
//     next({name:'Home'})
//   }
// })

export default router
