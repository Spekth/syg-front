import Vue from 'vue'
import Vuex, { Store } from 'vuex'

// decode jwt
import jwt_decode from 'jwt-decode'
import router from '../router'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token : '',
    userDB: ''
  },
  /*
   1. DECLARO LAS DOS VARIABLES token y userDB
   2. * Se crea una mutation donde almaceno el token el la variable token
      * Se crea una mutation donde almaceno en userDB el token
   3. Creo una action llamada saveUser guardo en la variable token el payload
      * en la variable userDB guardo el token decodificado
      * guardo en el localstorage el token 
   4. Creo una action llamada logout limpio el token y el userDB y elimino el token del localstorage
   5. Creo una action donde verifico si existe el token y lo guardo ademas de guardar el token decoficado
      * Si no existe el token establesco el userDB vacio
  */
  mutations: {
    getUser(state,payload){
     state.token = payload;
     if(payload === ''){
      state.userDB = ''
     }else{
       state.userDB = jwt_decode(payload);
     }
     state.userDB.role === 'INVERSIONISTA' 
        ? router.push({name:'ProjectInv'})
        : router.push({name:'ProjectEnt'})
    },
  },
  actions: {
    // Save userDB and token 
    saveUser({commit}, payload){
      localStorage.setItem('token',payload);
      commit('getUser',payload);
    },
    logout({commit}){
      commit('getUser','');
      localStorage.removeItem('token');
      router.push({name:'Home'}).catch(error=>{
        if(error.name!='NavegationDuplicated') throw error;
      })
    },
    // autoLogin
    readToken({commit}){
     const token = localStorage.getItem('token');
     if(token){
       commit('getUser',token);
     }else{
       commit('getUser','');
     }
    }
  },
  getters:{
    isActive: state => !!state.token 
  },
  modules: {
  }
})
