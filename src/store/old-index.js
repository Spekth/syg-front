import Vue from 'vue'
import Vuex, { Store } from 'vuex'

// decode jwt
import jwt_decode from 'jwt-decode'
import router from '../router'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token : '',
    userDB: ''
  },
  mutations: {
    // El payload es el token con la info
    setToken(state,payload){
      state.token = payload;
    },
    // getUser(state,payload) ORIGINAL{
    getUser(state,payload){
      state.token = payload;
      if(payload === ''){
        state.userDB = ''
      }else{
        state.userDB = jwt_decode(payload);
      }
     
      state.userDB.role === 'INVERSIONISTA' ? router.push({name:'ProjectInv'}) : router.push({name: 'ProjectEnt'})
    },
  },
  actions: {
    saveUser({commit}, payload){
       localStorage.setItem('token',payload);
       commit('getUser',payload)
      
    },
    logout({commit}){
      commit('getUser','');
      localStorage.removeItem('token');  
      router.push({name:'Home'})
    },
    readToken({commit}){
      const token = localStorage.getItem('token');
      if(token){
        commit('getUser',token)
      }else{
        commit('getUser','')
      }
    }
  },
  getters:{
    isActive: state => !!state.token 
  },
  modules: {
  }
})
